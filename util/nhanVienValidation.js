/**
 * Hàm kiểm tra rỗng dựa vào giá trị người dùng nhập
 * @param {*} value value là giá trị người dùng nhập
 * @param {*} selectorError selector hiển thị lỗi cho giá trị đó
 * @param {*} name tên thuộc tính bị lỗi khi hiển thị
 * @returns trả về giá trị hợp lệ(true) hoặc không hợp lệ(false)
 */

function kiemTraRong(value, selectorError, name) {
  if (value === "") {
    document.querySelector(selectorError).innerHTML =
      name + "không được bỏ trống!";
    return false;
  }
  document.querySelector(selectorError).innerHTML = "";
  return true;
}

function kiemTraTatCaKyTu(value, selectorError, name) {
  var regexLetter = /^[A-Z a-z]+$/; //Nhập các ký tự từ A-z a-z không chứa các ký tự khác
  if (regexLetter.test(value)) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  }
  document.querySelector(selectorError).innerHTML = name + "phải là chữ cái!";
  return false;
}

function kiemTraEmail(value, selectorError, name) {
  var regexEmail =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (regexEmail.test(value)) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  }
  document.querySelector(selectorError).innerHTML =
    name + "không đúng định dạng!";
  return false;
}
function kiemTraSo(value, selectorError, name) {
  var regexNumber = /^[0-9]+$/;
  if (regexNumber.test(value)) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  }
  document.querySelector(selectorError).innerHTML = name + "tất cả là số!";
  return false;
}

function kiemTraDoDai(value, selectorError, minLength, maxLength) {
  if (value.length > maxLength || value.length < minLength) {
    document.querySelector(selectorError).innerHTML =
      "Độ dài từ " + minLength + " - " + maxLength + " ký tự";
    return false;
  }
  document.querySelector(selectorError).innerHTML = "";
  return true;
}

function kiemTraGiaTri(value, selectorError, minValue, maxValue) {
  if (value > maxValue || value < minValue) {
    document.querySelector(selectorError).innerHTML =
      "Giá trị từ " + minValue + " - " + maxValue;
    return false;
  }
  document.querySelector(selectorError).innerHTML = "";
  return true;
}

// pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
function kiemTraPassword(value, selectorError, name) {
  var regexPassword =
    /^(?=.*\d)(?=.*[~`!@#$%^&*(+={}[|\/:;"'<>,.?])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z~`!@#$%^&*(+={}[|\/:;"'<>,.?]+$/;
  if (regexPassword.test(value)) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  }
  document.querySelector(selectorError).innerHTML =
    name + "phải chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt";
  return false;
}
