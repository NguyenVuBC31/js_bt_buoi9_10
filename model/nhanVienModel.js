function NhanVien() {
  this.tknv = "";
  this.name = "";
  this.email = "";
  this.password = "";
  this.datepicker = "";
  this.luongCB = "";
  this.chucVu = "";
  this.tinhTongLuong = function () {
    if (this.chucVu === "Sếp") {
      return this.luongCB * 3;
    } else if (this.chucVu === "Trưởng phòng") {
      return this.luongCB * 2;
    } else if (this.chucVu === "Nhân viên") {
      return this.luongCB;
    }
  };
  this.gioLam = "";
  this.xepLoaiNhanVien = function () {
    if (this.gioLam >= 192) {
      return "Nhân viên xuất sắc";
    } else if (this.gioLam >= 176) {
      return "Nhân viên giỏi";
    } else if (this.gioLam >= 160) {
      return "Nhân viên khá";
    } else if (this.gioLam < 160) {
      return "Nhân viên trung bình";
    }
  };
}
