var mangNhanVien = [];

document.getElementById("btnThemNV").onclick = function () {
  var nhanVien = new NhanVien();
  nhanVien.tknv = document.getElementById("tknv").value;
  nhanVien.name = document.getElementById("name").value;
  nhanVien.email = document.getElementById("email").value;
  nhanVien.password = document.getElementById("password").value;
  nhanVien.datepicker = document.getElementById("datepicker").value;
  nhanVien.luongCB = document.getElementById("luongCB").value * 1;
  nhanVien.chucVu = document.getElementById("chucvu").value;
  nhanVien.gioLam = document.getElementById("gioLam").value * 1;
  var valid = true;
  //Kiểm tra dữ liệu trước khi thêm vào mảng
  //Kiểm tra rỗng
  valid &=
    kiemTraRong(nhanVien.tknv, "#error_required_tknv", "Tài khoản nhân viên ") &
    kiemTraRong(nhanVien.name, "#error_required_name", "Tên nhân viên ") &
    kiemTraRong(nhanVien.email, "#error_required_email", "Email ") &
    kiemTraRong(nhanVien.password, "#error_required_password", "Password ") &
    kiemTraRong(nhanVien.luongCB, "#error_required_luongCB", "Lương cơ bản ") &
    kiemTraRong(nhanVien.chucVu, "#error_required_chucvu", "Chức vụ ") &
    kiemTraRong(nhanVien.gioLam, "#error_required_gioLam", "Giờ làm ");
  //Kiểm tra chữ
  valid &= kiemTraTatCaKyTu(
    nhanVien.name,
    "#error_all_letter_name",
    "Tên nhân viên "
  );
  //Kiểm tra số
  valid &= kiemTraSo(
    nhanVien.tknv,
    "#error_all_number_tknv",
    "Tài khoản nhân viên "
  );
  //Kiểm tra email
  valid &= kiemTraEmail(nhanVien.email, "#error_email", "Email ");
  //Kiểm tra độ dài
  valid &=
    kiemTraDoDai(nhanVien.tknv, "#error_min_max_tknv", 4, 6) &
    kiemTraDoDai(nhanVien.password, "#error_min_max_password", 6, 10);
  //Kiểm tra giá trị
  valid &=
    kiemTraGiaTri(nhanVien.luongCB, "#error_value_luongCB", 1000000, 20000000) &
    kiemTraGiaTri(nhanVien.gioLam, "#error_value_gioLam", 80, 200);
  //Kiểm tra Password
  valid &= kiemTraPassword(
    nhanVien.password,
    "#error_valid_password",
    "Password"
  );

  if (!valid) {
    return;
  }

  // mỗi lần nhập liệu và bấm nút xác nhận thì thêm sinh viên vào mảng
  mangNhanVien.push(nhanVien);
  // lưu mảng vào Storage
  luuStorage(mangNhanVien, "mangNhanVien");

  //Gọi hàm tạo table sinhVien
  var html = renderNhanVien(mangNhanVien);
  document.querySelector("tbody").innerHTML = html;
};

function renderNhanVien(arrNhanVien) {
  //param: input: arrSinhVien

  var html = ""; //output: string html
  for (var i = 0; i < arrNhanVien.length; i++) {
    var nv = arrNhanVien[i]; //Mỗi lần duyệt lấy ra 1 object sinhVien từ mảng{maSinhVien:"1",tenSinhVien:'',..}
    html += `
      <tr>
          <td>${nv.tknv}</td>
          <td>${nv.name}</td>
          <td>${nv.email}</td>
          <td>${nv.datepicker}</td>
          <td>${nv.chucVu}</td>
          <td>${nv.tinhTongLuong()}</td>
          <td>${nv.xepLoaiNhanVien()}</td>
          <td>
          <button class="btn btn-danger">Xóa</button>
          </td>
      </tr>
      `;
  }
  return html;
}
//Lưu dữ liệu xuống Storage
function luuStorage(content, storageName) {
  var sContent = JSON.stringify(content); //Biến đổi object hoặc array thành chuỗi
  localStorage.setItem(storageName, sContent); //Đem chuỗi đó lưu vào localStorage
}
//Lấy dữ liệu từ Storage
function layStorage(storageName) {
  var output; //undefined
  if (localStorage.getItem(storageName)) {
    output = JSON.parse(localStorage.getItem(storageName));
    for (var index = 0; index < output.length; index++) {
      var item = output[index];
      var nv = new NhanVien(
        item.tknv,
        item.name,
        item.email,
        item.password,
        item.datepicker,
        item.luongCB,
        index.chucVu,
        item.gioLam
      );
      mangNhanVien.push(nv);
    }
  }
  // return output; //underfined
}
//Hàm sẽ chạy khi giao diện vừa load lên
window.onload = function () {
  //Lấy dữ liệu từ storage
  var content = layStorage("mangNhanVien");
  //Nếu có dữ liệu trong storage thì xử lý in ra giao diện html cho table
  if (content) {
    mangNhanVien = content;
    var html = renderNhanVien(content);
    document.querySelector("tbody").innerHTML = html;
  }
};
